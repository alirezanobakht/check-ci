from variables import SEIIMARK

with open("index.html", "w") as f:
    f.write(f"""
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>We ❤️ SE II</title>
    </head>
    <body>
        <h1>Your Software Engineering II mark is {SEIIMARK} 🎉</h1>
    </body>
    </html>
    """)